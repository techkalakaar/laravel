<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* Dashboard Route */
Route::get('/', 'DashboardController@index')->name('home');

/* Login Route */
Route::get('/login', function(){
	return view('sessions.login');
})->name('login');

/* Login Check */
Route::post('/login', 'SessionController@checkLogin');


/*  Logout  */
Route::get('/logout', 'SessionController@destroy');

//Auth::routes();


/*  Clients Route */
Route::get('/clients','ClientsController@view');

/* Clients Add */
Route::get('/clients/add', 'ClientsController@add');

/* Clients Save */
Route::post('/clients/add', 'ClientsController@save');

/* Clients Get Edit Page */
Route::get('/clients/edit/{id}', 'ClientsController@showSingle');

/* Clients update single */
Route::post('/clients/edit/{id}','ClientsController@updateSingle');

/* Clients delete entry */
Route::post('/clients/delete/{id}','ClientsController@delete');

/* Clients Excel Upload */
Route::post('/clients/ExcelUpload','ClientsController@ExcelUpload');

/* Clients Brokerage */
Route::get('/clients/brokerage', 'ClientBrokerageController@index');

/* Clients Brokerage Add */
Route::get('/clients/brokerage/add','ClientBrokerageController@add');

/* Clients Brokerage Add */
Route::post('/clients/brokerage/add','ClientBrokerageController@save');

/* Clients Brokerage excel upload */
Route::post('/clients/brokerage/ExcelUpload','ClientBrokerageController@ExcelUpload');


/* Brokerages Views*/
Route::get('/brokerage','BrokerageChargesController@index');

/* Brokerages Update */
Route::post('/brokerages/edit/{id}','BrokerageChargesController@update');

/* Deals Route */
Route::get('/deals','DealsController@index');

/* Deals Add */
Route::get('/deals/add','DealsController@add');

/* Deals save */
Route::post('/deals/add','DealsController@save');

/* Deals Excel Upload */
Route::post('/deals/ExcelUpload','DealsController@ExcelUpload');

/* Calculate */
Route::post('/deals/calc','DealsController@calculate');

/* Deals get Data */
Route::post('/deals/get-data','DealsController@getData');


/* Deals Calc Sell */
Route::post('/deals/calcSell', 'DealsController@calcSell');

/* Transaction View */
Route::get('/transaction','TransactionsController@index');

/* Calculate Single Transaction */
Route::get('/deals/calculate', 'DealsController@viewCalc');

