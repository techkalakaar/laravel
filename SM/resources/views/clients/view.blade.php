@extends('layout.master')


@section('content')

<div class="row">
	
	<div class="col-md-12">
		
		<div>
			
			<button type="button" class="btn btn-md btn-primary" onclick="uploadExcel()">Excel Upload</button>

<hr/>
		</div>

		<table class="table table-striped table-dark">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Account Id</th>
					<th scope="col">Name</th>
					<th scope="col">Number</th>
					<th scope="col">Email</th>
					<th scope="col">City</th>
					<th scope="col">Investment Amount</th>
					<th scope="col">Investment Date</th>
					<th scope="col">Edit</th>
					<th scope="col">Delete</th>
				</tr>
			</thead>
			<tbody>

				@if(count($clients))
					@foreach($clients as $client)
					<tr>
						<th scope="row">{{ $client->id }}</th>
						<td>{{ $client->account_id }}</td>
						<td>{{ $client->name }}</td>
						<td>{{ $client->number }}</td>
						<td>{{ $client->email }}</td>
						<td>{{ $client->city }}</td>
						<td>{{ $client->investment_amount }}</td>
						<td>{{ $client->investment_date }}</td>
						<td><a href="/clients/edit/{{ $client->id }}">Edit</a></td>
						<td><a href="javascript:void(0)" onclick="javascript:showConfirmModal({{ $client->id }})">Delete</a></td>
					</tr>
					@endforeach
				@endif
			</tbody>
		</table>

		{{ $clients->links('clients.pagination') }}

	</div>

</div>

@include('layout.confirm-modal')
@include('layout.excel-upload')

@endsection

@section('script')

<script>
	

function showConfirmModal(id){
	$('#form-delete').attr('action','');

	$('#form-delete').attr('action','/clients/delete/'+id);

	$('#confirm').modal('show');
}

function uploadExcel(){
	$('#form-upload').attr('action','');

	$('#form-upload').attr('action','/clients/ExcelUpload');

	$('#excelUpload').modal('show');
}

</script>

@endsection