@extends('layout.master')


@section('content')

<div class="row">
	
	<div class="col-md-12">
		
		<div>
			
			<button type="button" class="btn btn-md btn-primary" onclick="uploadExcel()">Excel Upload</button>

<hr/>
		</div>

		<table class="table table-striped table-dark">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Account Id</th>
					<th scope="col">Cash Delivery Rate</th>
					<th scope="col">Cash Intraday Rate</th>
					<th scope="col">Future Rate</th>
					<th scope="col">Option Rate</th>
					<th scope="col">Currency Future Rate</th>
					<th scope="col">Currency Option Rate</th>
					<th scope="col">Commodities</th>
					<th scope="col">With Effects From</th>
				</tr>
			</thead>
			<tbody>

				@if(count($clientsBrokerage))
					@foreach($clientsBrokerage as $clientBrokerage)
					<tr>
						<th scope="row">{{ $clientBrokerage->id }}</th>
						<td>{{ $clientBrokerage->account_id }}</td>
						<td>{{ $clientBrokerage->cash_delivery_rate }}</td>
						<td>{{ $clientBrokerage->cash_intraday_rate }}</td>
						<td>{{ $clientBrokerage->future_rate }}</td>
						<td>{{ $clientBrokerage->option_rate }}</td>
						<td>{{ $clientBrokerage->currency_future_rate }}</td>
						<td>{{ $clientBrokerage->currency_option_rate }}</td>
						<td>{{ $clientBrokerage->commodities }}</td>
						<td>{{ $clientBrokerage->wef }}</td>
					</tr>
					@endforeach
				@endif
			</tbody>
		</table>

{{ $clientsBrokerage->links('clients.brokerage.pagination') }}

	</div>

</div>

@include('layout.excel-upload')

@endsection

@section('script')

<script>

function uploadExcel(){
	$('#form-upload').attr('action','');

	$('#form-upload').attr('action','/clients/brokerage/ExcelUpload');

	$('#excelUpload').modal('show');
}

</script>

@endsection