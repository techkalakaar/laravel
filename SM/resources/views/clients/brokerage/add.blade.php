@extends('layout.master')


@section('content')

<div class="row">
<div class="col-md-12">
	<form action="/clients/brokerage/add" method="post">

		{{ csrf_field() }}

		<div class="row">
			<div class="col-md-6">

				<div class="form-group">
					
					<label for="inputAccountId">Account ID</label>
					<input type="text" name="inputAccountId" id="inputAccountId" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputCashDeliveryRate">Cash Delivery Rate</label>
					<input type="text" name="inputCashDeliveryRate" id="inputCashDeliveryRate" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputCashIntradayRate">Cash Intraday Rate</label>
					<input type="text" name="inputCashIntradayRate" id="inputCashIntradayRate" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputFutureRate">Future Rate</label>
					<input type="text" name="inputFutureRate" id="inputFutureRate" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputOptionRate">Option Rate</label>
					<input type="text" name="inputOptionRate" id="inputOptionRate" class="form-control" required />

				</div>

			</div>

			<div class="col-md-6">

				<div class="form-group">
					
					<label for="inputCurrencyFutureRate">Currency Future Rate</label>
					<input type="text" name="inputCurrencyFutureRate" id="inputCurrencyFutureRate" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputCurrencyOptionRate">Currency Option Rate</label>
					<input type="text" name="inputCurrencyOptionRate" id="inputCurrencyOptionRate" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputCommodities">Commodities</label>
					<input type="text" name="inputCommodities" id="inputCommodities" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputWithEffectsFrom">With Effects From</label>
					<input type="date" name="inputWithEffectsFrom" id="inputWithEffectsFrom" class="form-control" required />

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="form-group">
					
					<button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>

				</div>
			</div>
		</div>
	</form>
</div>
</div>

@include('layout.errors')

@endsection