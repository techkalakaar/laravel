@extends('layout.master')


@section('content')


<div class="row">
<div class="col-md-12">
	@if(count($clients))
	<form action="/clients/edit/{{ $clients['id'] }}" method="post">
		{{ csrf_field() }}

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					
					<label for="inputClientId">Client ID</label>
					<input type="text" name="inputClientId" id="inputClientId" class="form-control" value="{{ $clients['client_id'] }}" required />

				</div>

				<div class="form-group">
					
					<label for="inputAccountId">Account ID</label>
					<input type="text" name="inputAccountId" id="inputAccountId" class="form-control" value="{{ $clients['account_id'] }}" required />

				</div>

				<div class="form-group">
					
					<label for="inputName">Name</label>
					<input type="text" name="inputName" id="inputName" class="form-control" value="{{ $clients['name'] }}" required />

				</div>

				<div class="form-group">
					
					<label for="inputNumber">Number</label>
					<input type="number" name="inputNumber" id="inputNumber" class="form-control"  value="{{ $clients['number'] }}" required />

				</div>

			</div>

			<div class="col-md-6">
				<div class="form-group">
					
					<label for="inputEmail">Email</label>
					<input type="email" name="inputEmail" id="inputEmail" class="form-control" value="{{ $clients['email'] }}" required />

				</div>

				<div class="form-group">
					
					<label for="inputCity">City</label>
					<input type="text" name="inputCity" id="inputCity" class="form-control" value="{{ $clients['city'] }}" required />

				</div>

				<div class="form-group">
					
					<label for="inputInvestmentAmount">Investment Amount</label>
					<input type="number" name="inputInvestmentAmount" id="inputInvestmentAmount" class="form-control" value="{{ $clients['investment_amount'] }}" required />

				</div>

				<div class="form-group">
					
					<label for="inputInvestmentDate">Investment Date</label>
					<input type="date" name="inputInvestmentDate" id="inputInvestmentDate" class="form-control" value="{{ $clients['investment_date'] }}" required />

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="form-group">
					
					<button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>

				</div>
			</div>
		</div>
	</form>
	@endif
</div>
</div>

@include('layout.errors')

@endsection