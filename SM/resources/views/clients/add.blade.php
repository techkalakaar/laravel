@extends('layout.master')


@section('content')

<div class="row">
<div class="col-md-12">
	<form action="/clients/add" method="post">

		{{ csrf_field() }}

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					
					<label for="inputClientId">Client ID</label>
					<input type="text" name="inputClientId" id="inputClientId" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputAccountId">Account ID</label>
					<input type="text" name="inputAccountId" id="inputAccountId" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputName">Name</label>
					<input type="text" name="inputName" id="inputName" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputNumber">Number</label>
					<input type="number" name="inputNumber" id="inputNumber" class="form-control" required />

				</div>

			</div>

			<div class="col-md-6">
				<div class="form-group">
					
					<label for="inputEmail">Email</label>
					<input type="email" name="inputEmail" id="inputEmail" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputCity">City</label>
					<input type="text" name="inputCity" id="inputCity" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputInvestmentAmount">Investment Amount</label>
					<input type="number" name="inputInvestmentAmount" id="inputInvestmentAmount" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputInvestmentDate">Investment Date</label>
					<input type="date" name="inputInvestmentDate" id="inputInvestmentDate" class="form-control" required />

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="form-group">
					
					<button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>

				</div>
			</div>
		</div>
	</form>
</div>
</div>

@include('layout.errors')

@endsection