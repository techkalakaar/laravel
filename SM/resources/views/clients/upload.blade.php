@extends('layout.master')

@section('content')

<form action="/clients/ExcelUpload" id="form-upload" method="post">
    {{csrf_field()}}
    <input type="file" name="file" id="file" value="">
    <button type="submit" class="btn btn-sm btn-primary" id="">Submit</button>
</form>

@endsection