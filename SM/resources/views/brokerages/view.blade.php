@extends('layout.master')

@section('content')

<div class="row">
	
	<div class="col-md-12">

		<table class="table table-striped table-dark">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Type</th>
					<th scope="col">Name</th>
					<th scope="col">Rate</th>
					<th scope="col">Levied On</th>
					<th scope="col">Edit</th>
				</tr>
			</thead>
			<tbody>

				@if(count($cash))
					@foreach($cash as $c)
					<tr>
						<th scope="row">{{ $c->id }}</th>
						<td>{{ $c->type }}</td>
						<td>{{ $c->name }}</td>
						<td>{{ $c->rate }}</td>
						<td>{{ $c->levied_on }}</td>
						<td>{{ $c->option_rate }}</td>
						<td><a href="javascript:void(0)" onclick="javascript:showEdit({{ $c->id }},'{{ $c->rate }}')">Edit</a></td>
					</tr>
					@endforeach
				@endif
			</tbody>
		</table>

		{{ $cash->links('brokerages.pagination') }}

	</div>

</div>

@include('brokerages.update')

@endsection

@section('script')

<script type="text/javascript">

function showEdit(id, rate){

	$('#brokerageUpdateForm').attr('action', '');

	$('#brokerageUpdateForm').attr('action', '/brokerages/edit/'+id);

	$('#inputUpdateRate').val('');

	$('#inputUpdateRate').val(rate);

	$('#brokerageUpdateModal').modal('show');



	//console.log(rate);

}
</script>

@endsection