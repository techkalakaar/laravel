<div class="modal" id="calcModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Excel Upload</h4>
            </div>
            <div class="modal-body">
                <form action="/deals/calc" id="form-calc" method="post">
                    {{csrf_field()}}
                    <input type="date" name="deals-calc-date" id="deals-calc-date" class="form-control" />
                    <button type="submit" class="btn btn-sm btn-primary" id="">Submit</button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>