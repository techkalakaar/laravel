@extends('layout.master')

@section('content')

<div class="row">
<div class="col-md-12">
	<form action="/deals/add" method="post">

		{{ csrf_field() }}

		<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					
					<label for="inputUserId">User Id</label>
					<input type="text" name="inputUserId" id="inputUserId" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputExchangeSegment">Exchange Segment</label>
					<input type="text" name="inputExchangeSegment" id="inputExchangeSegment" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputAccountId">Account Id</label>
					<input type="text" name="inputAccountId" id="inputAccountId" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputSymbol">Symbol</label>
					<input type="text" name="inputSymbol" id="inputSymbol" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputExpiryDate">Expiry Date</label>
					<input type="text" name="inputExpiryDate" id="inputExpiryDate" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputStrikePrice">Strike Price</label>
					<input type="text" name="inputStrikePrice" id="inputStrikePrice" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputOptionType">Option Type</label>
					<input type="text" name="inputOptionType" id="inputOptionType" class="form-control" required />

				</div>

			</div>
			<div class="nikul"></div>
			<div class="col-md-6">
				<div class="form-group">
					
					<label for="inputBuySell">Buy/Sell</label>
					<input type="text" name="inputBuySell" id="inputBuySell" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputProductType">Product Type</label>
					<input type="text" name="inputProductType" id="inputProductType" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputTradeQty">Trade Qty</label>
					<input type="text" name="inputTradeQty" id="inputTradeQty" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputTradePrice">Trade Price</label>
					<input type="text" name="inputTradePrice" id="inputTradePrice" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputTradeStatus">Trade Status</label>
					<input type="text" name="inputTradeStatus" id="inputTradeStatus" class="form-control" required />

				</div>

				<div class="form-group">
					
					<label for="inputDealDate">Deal Date</label>
					<input type="date" name="inputDealDate" id="inputDealDate" class="form-control" required />

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="form-group">
					
					<button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>

				</div>
			</div>
		</div>
	</form>
</div>
</div>

@endsection