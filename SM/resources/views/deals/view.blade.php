@extends('layout.master')


@section('content')


<div class="row">
	
	<div class="col-md-12">
		
		<div>
			
			<button type="button" class="btn btn-md btn-primary" onclick="uploadExcel()">Excel Upload</button>

			<button type="button" class="btn btn-md btn-primary" onclick="openCalc()">Calculate</button>

<hr/>
		</div>

		<table class="table table-striped table-dark">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">User Id</th>
					<th scope="col">Exchange segment</th>
					<th scope="col">Account Id</th>
					<th scope="col">Symbol</th>
					<th scope="col">Expiry Date</th>
					<th scope="col">Strike Price</th>
					<th scope="col">Option type</th>
					<th scope="col">Buy/Sell</th>
					<th scope="col">Product Type</th>
					<th scope="col">Trade Qty.</th>
					<th scope="col">Trade Price</th>
					<th scope="col">Trade Status</th>
					<th scope="col">Status</th>
					<th scope="col">Deal Date</th>
				</tr>
			</thead>
			<tbody>


				@if(count($deals))
					@foreach($deals as $deal)
					<tr>
						<th scope="row">{{ $deal->id }}</th>
						<td>{{ $deal->user_id }}</td>
						<td>{{ $deal->exchange_segment }}</td>
						<td>{{ $deal->account_id }}</td>
						<td>{{ $deal->symbol }}</td>
						<td>{{ $deal->expiry_date }}</td>
						<td>{{ $deal->strike_price }}</td>
						<td>{{ $deal->option_type }}</td>
						<td>{{ $deal->buy_or_sell }}</td>
						<td>{{ $deal->product_type }}</td>
						<td>{{ $deal->trade_qty }}</td>
						<td>{{ $deal->trade_price }}</td>
						<td>{{ $deal->trade_status }}</td>
						<td>
							@if($deal->status == 1)
								<div class="alert alert-success" role="alert">Done</div>
							@else
								<a href="javascript:void(0)" onclick="javascript:showSellModal({{ $deal->id }})" class="alert alert-danger" role="alert">Pending</a>
							@endif
						</td>
						<td>{{ $deal->deal_date }}</td>
					</tr>
					@endforeach
				@endif
			</tbody>
		</table>

		{{ $deals->links('deals.pagination') }}

	</div>

</div>

@include('layout.errors')
@include('deals.excel-upload')
@include('deals.calc-modal')

@include('deals.sell-modal')

@endsection

@section('script')

<script>
	
function uploadExcel(){
	$('#form-upload').attr('action','');

	$('#form-upload').attr('action','/deals/ExcelUpload');

	$('#excelUpload').modal('show');
}

function openCalc(){
	$('#calcModal').modal('show');
}

function showSellModal(id){

	$('input[name="sellID"]').val(id)

	$.ajax({

		headers: {
		      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"POST",
		dataType:"JSON",
		url:"/deals/get-data",
		data:{'id': id},
		success: function(response){
			if(!$.trim(response)==''){

				var html = "<table style='width:100%;'>";
				html += "<tr><td>Account ID</td><td>Script Name</td><td>Quantity</td><td>Purchase Rate</td><td>Select</td><td>Quantity</td></tr>";
				for(var i= 0; i<response.length; i++){
					html +="<tr><td>"+response[i]["account_id"]+"</td><td>"+response[i]["script_name"]+"</td><td>"+response[i]["qty"]+"</td><td>"+response[i]["purchase_rate"]+"</td><td><input type='checkbox' name='sellListOption' value='"+response[i]["id"]+"' /></td><td><input type='number' name='sellQuantity' style='color:#000;' /></td></tr>";
					//console.log(response[i]["id"]);
				}
				html += "</table>";

				$("#sell-table").html(html);
				//console.log(html);
			}
			else{
				console.log("No record available");
			}
		}

	});

	$('#modal-info-sell').modal('show');
}

function calcSell(){
    //var id = $("input[name='sellListOption']:checked").val();
    var checkedCheckboxesValues = 
    $('input:checkbox[name="sellListOption"]:checked')
        .map(function() {
            return $(this).val();
        }).get();
    var txtNumberValues = 
    $('input[name="sellQuantity"]')
        .map(function() {
            if($(this).val() != "")
                return $(this).val();
        }).get();
        
    var sellId = $('input[name="sellID"]').val();
    
    $.ajax({

		headers: {
		      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method:"POST",
		dataType:"JSON",
		url:"/deals/calcSell",
		data:{id:sellId, purchaseId:checkedCheckboxesValues.join(","), sellQuantity: txtNumberValues.join(","), sellDate:$("#sellDate").val()},
		success: function(response){
			if(!$.trim(response)==''){

				console.log(response);
			}
			else{
				console.log("No record available");
			}
		}

	});
}

</script>


@endsection