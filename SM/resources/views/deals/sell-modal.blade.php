<!-- /.modal -->
<div class="modal modal-info fade" id="modal-info-sell">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Sell Calculation</h4>
      </div>
      <div class="modal-body" style="background-color: #000 !important;">
          Date: <input type="date" name="sellDate" id="sellDate" style="color:#000;" />
        <div id="sell-table">
            
        </div>
        <input type="hidden" value="" name="sellID">
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-outline" onclick="calcSell()">Submit</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->