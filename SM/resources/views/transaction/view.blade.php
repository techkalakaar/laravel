@extends('layout.master')

@section('content')

<div class="row">
	
	<div class="col-md-12">
		
		<div>
			
			<button type="button" class="btn btn-md btn-primary" onclick="uploadExcel()">Excel Upload</button>

			<button type="button" class="btn btn-md btn-primary" onclick="openCalc()">Calculate</button>

<hr/>
		</div>

		<table class="table table-striped table-dark">
			<thead>
				<tr>
					<th scope="col">#</th>
					<th scope="col">Account Id</th>
					<th scope="col">segment</th>
					<th scope="col">Buy Date</th>
					<th scope="col">Buy/Sale</th>
					<th scope="col">Qty</th>
					<th scope="col">Script Name</th>
					<th scope="col">Purchase Rate</th>
					<th scope="col">Purchase Brokerage Taxes</th>
					<th scope="col">Sell Date</th>
					<th scope="col">Sell Rate</th>
					<th scope="col">Sale Brokerage Taxes</th>
					<th scope="col">Open/Close</th>
					<th scope="col">SL/Trail</th>
					<th scope="col">Target</th>
					<th scope="col">Margin</th>
					<th scope="col">Profit/Loss</th>
					<th scope="col">Timeframe</th>
					<th scope="col">Edit</th>
					<th scope="col">Delete</th>
				</tr>
			</thead>
			<tbody>


				@if(count($data))
					@foreach($data as $d)
					<tr>
						<th scope="row">{{ $d->id }}</th>
						<td>{{ $d->account_id }}</td>
						<td>{{ $d->segment }}</td>
						<td>{{ $d->buy_date }}</td>
						<td>{{ $d->buy_or_sell }}</td>
						<td>{{ $d->qty }}</td>
						<td>{{ $d->script_name }}</td>
						<td>{{ $d->purchase_rate }}</td>
						<td>{{ $d->purchase_tax }}</td>
						<td>{{ $d->sell_date }}</td>
						<td>{{ $d->sell_rate }}</td>
						<td>{{ $d->sell_tax }}</td>
						<td>{{ $d->open_or_close }}</td>
						<td>{{ $d->sl_or_trail }}</td>
						<td>{{ $d->target }}</td>
						<td>{{ $d->profit_or_loss }}</td>
						<td>{{ $d->timeframe }}</td>
						<td>
							<a href="/transaction/edit/{{$d->id}}" class="">Edit</a>
						</td>
						<td>
							<a href="/transaction/delete/{{$d->id}}" class="">Delete</a>
						</td>
					</tr>
					@endforeach
				@endif
			</tbody>
		</table>

		{{ $data->links('transaction.pagination') }}

	</div>

</div>

@endsection