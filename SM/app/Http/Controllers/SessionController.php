<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class SessionController extends Controller
{

	public function __construct(){

		$this->middleware('guest', ['except'=>'destroy']);

	}

    public function checkLogin(){

    	$this->validate(request(),

    		[
    			'inputUsername'=>'required',
    			'inputPassword'=>'required'
    		]
    	);


    	if(!auth()->attempt(['username'=>request('inputUsername'), 'password'=>request('inputPassword')])){
    		return back()->withErrors([
    			'message' => 'Invalid Credentials'
    		]);
    	}
    	else{
    		$userId = \Auth::id();
    		\Auth::loginUsingId($userId);
    		return redirect()->home();
    	}

    	/*$user = User::whereUsername(request('inputUsername'))->wherePassword(bcrypt(request('inputPassword')))->first();

    	if($user){
    		\Auth::loginUsingId($user->id);
    		return redirect()->home();
    	}

    	return back()->withErrors([
    		'message'=>'Invali Credentials'
    	]);*/
    }


    public function destroy(){

    	return redirect('/login')->with(\Auth::logout());

    }

}
