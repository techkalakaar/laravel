<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Deals;
use Carbon\Carbon;
use App\ClientBrokerage;
use App\Transactions;

use Excel;

class DealsController extends Controller
{
    public function __construct(){

        $this->middleware('auth');

    }

    public function index(Request $request){

    	$deals = Deals::paginate(10);

    	return view('/deals.view',compact('deals'))->with('i', ($request->input('page', 1)-1)*10);

    }

    public function add(){

    	return view('/deals.add');

    }

    public function save(){

    	Deals::create([

    		'user_id'=>auth()->id(),
    		'exchange_segment'=>request('inputExchangeSegment'),
    		'account_id'=>request('inputAccountId'),
    		'symbol'=>request('inputSymbol'),
    		'expiry_date'=>request('inputExpiryDate'),
    		'strike_price'=>request('inputStrikePrice'),
    		'option_type'=>request('inputOptionType'),
    		'buy_or_sell'=>request('inputBuySell'),
    		'product_type'=>request('inputProductType'),
    		'trade_qty'=>request('inputTradeQty'),
    		'trade_price'=>request('inputTradePrice'),
    		'trade_status'=>request('inputTradeStatus'),
    		'status'=>0,
    		'deal_date'=>request('inputDealDate'),
    		'created_ip'=>\Request::ip(),
    		'created_at'=>Carbon::now()->toDateTimeString(),
    		'update_at'=>Carbon::now()->toDateTimeString()

    	]);

    	session()->flash('alert-success', 'Data saved successfully!!!');

    	return redirect('/deals/add');

    }


    public function ExcelUpload(Request $request){

        if($request->hasFile('excelFile')){

//            dd('entered');

            $path = $request->file('excelFile')->getRealPath();

            $data = Excel::load($path, function($reader){})->get();

            if(!empty($data) && $data->count()){
                foreach($data as $key => $value){

                    $insert[] = [

                        'user_id'=>auth()->id(),
                        'exchange_segment'=>$value->exchange_segment,
                        'account_id'=>$value->account_id,
                        'symbol'=>$value->symbol,
                        'expiry_date'=>$value->expiry_date,
                        'strike_price'=>$value->strike_price,
                        'option_type'=>$value->option_type,
                        'buy_or_sell'=>$value->buy_or_sell,
                        'product_type'=>$value->product_type,
                        'trade_qty'=>$value->trade_qty,
                        'trade_price'=>$value->trade_price,
                        'trade_status'=>$value->trade_status,
                        'status'=>0,
                        'deal_date'=>request('deals-date'),
                        'created_ip'=>\Request::ip(),
                        'created_at'=>Carbon::now()->toDateTimeString(),
                        'updated_at'=>Carbon::now()->toDateTimeString()

                    ];

                }

                //dd($insert);

                if(!empty($insert)){

                    $excelInsert = Deals::insert($insert);

                    if($excelInsert){

                        session()->flash('alert-success', 'Data uploaded successfully!!');

                    }
                    else{

                        session()->flash('alert-danger', 'An error occured. Please try again!');

                    }

                }
            }

        }

        //session()->flash('alert-success', 'Data uploaded successfully!!');

        return redirect('/deals');

    }

    public function calculate(){

        $this->validate(request(),[

            'deals-calc-date'=>'required'

        ]);

        $a = array();
        $calculate_cash = array();
        
        $response = "";
        $deal_date = "";
        $current_date = request('deals-calc-date');

        //$total_brokerage = 0;

        $brokerage_old_arr = array();
        $brokerage_new_arr = array();

        $transaction_charges = 0;

        $total_brokerage = 0;

        $query_user_id = Deals::distinct()->where('deal_date','=',$current_date)->get(['account_id']);
        //dd($query_user_id);
        foreach ($query_user_id as $result_account_id) {
            $value = array();

            $query = Deals::where('account_id','=',$result_account_id->account_id)->where('deal_date','=',$current_date)->get();

            $i = 0;
            foreach ($query as $result) {
                
                $calculate_cash[$i]["ID"] = $result->id;
                $userid = $result->account_id;
                $type = $result->exchange_segment;
                $symbol = $result->symbol;
                $series_or_expiry = $result->expiry_date;
                $strike_price = $result->strike_price;
                $option_typ = $result->option_type;
                $trade_qty = $result->trade_qty;
                $trade_price = $result->trade_price;
                $buy_or_sell_db = $result->buy_or_sell;
                $deal_date = $result->deal_date;
                $avg_price = $trade_price;


                $net_buy_qty = $trade_qty;
                $buy_avg_price = $avg_price;
                $net_sell_qty = $trade_qty;
                $sell_avg_price = $avg_price;
                
                $turnover = 0;
                $stamp_duty_intraday = 0;
                $stamp_duty_delivery = 0;
                $transaction_charges_nse = 0;
                $transaction_charges_bse = 0;
                $security_transaction_tax_delivery = 0;
                $security_transaction_tax_intraday = 0;
                $sebi_turnover_fees = 0;
                $gst = 0;
                $buy_sale_amount = 0;
                $brokerage = 0;
                $exit_load = 0;
                $total_taxes = 0;
                $turnover_buy = 0;
                $turnover_sell = 0;
                $summ_stt_sd = 0;
                $stamp_duty_commodities = 0;
                $risk_management_fee = 0;
                $total_qty = 0;
                $margin = 0;
                $sell_rate = 0;
                $purchase_rate = 0;
                $profit_or_loss = 0;
                
                
                //$tot_brokerage = 0;
                $buy_or_sell = "";
                $open_or_close = "";
                $trade_type = "";
                $trade_on = "";
                $nse_bse = "";
                $buy_date = "";
                $sell_date = "";
                $timeframe = "M";
                $buy_brokerage = 0;
                $sell_brokerage = 0;
                
                //if($net_buy_qty != "" && $buy_avg_price != ""){
                if($buy_or_sell_db == "BUY"){
                    $turnover = $net_buy_qty*$buy_avg_price;
                    $turnover_buy = $turnover;
                    //$calculate_cash["Turnover buy"] = $turnover_buy;
                    $buy_sale_amount = $buy_avg_price;
                    $buy_or_sell = "BUY";
                    $buy_date = $deal_date;
                    $total_qty = $net_buy_qty;
                }
                //if($net_sell_qty != "" && $sell_avg_price != ""){
                if($buy_or_sell_db == "SELL"){
                    $turnover = $net_sell_qty*$sell_avg_price;
                    $turnover_sell = $turnover;
                    $buy_sale_amount = $sell_avg_price;
                    $buy_or_sell = "SELL";
                    $sell_date = $deal_date;
                    $total_qty = $net_sell_qty;
                }
                
                //if($net_buy_qty != "" && $buy_avg_price != "" && $net_sell_qty != "" && $sell_avg_price != ""){
                if($buy_or_sell_db == "BUY" && $buy_or_sell_db == "SELL"){
                    $turnover = $turnover_buy+$turnover_sell;
                    //$calculate_cash["Turnover buy+Sell"] = $turnover;
                    $buy_or_sell = "BUY/SELL";
                    $buy_date = $deal_date;
                    $sell_date = $deal_date;
                    $timeframe = "I";
                }
                
                $turn_over = $avg_price*$trade_qty;

                $query_brokerage = ClientBrokerage::limit(1)->where('account_id','=',$result->account_id)->where('wef','<=',Carbon::now()->format('Y-m-d'))->get();

                //$this->brokerageCalculation(1, 120000);

                //dd($query_brokerage[0]);
                //dd($query_brokerage[0]->id);

                if($type == "NSE" || $type == "BSE" || $type == "NFO"){
                    $trade_type = "Cash";
                }
                else if($type == "NSE-CDS" || $type == "BSE-CDS"){
                    $trade_on = "Currency";
                    if($type == "NSE-CDS")
                        $nse_bse = "NSE";
                    else if($type == "BSE-CDS")
                        $nse_bse = "BSE";
                }
                else if($type == "MCX" || $type == "NCDEX"){
                    $trade_type = "Commodities";
                }
                $open_or_close = "";
                //if($net_sell_qty != "" && $sell_avg_price != ""){
                if($buy_or_sell_db == "SELL"){
                    $open_or_close = "CLOSE";
                }
                //if($net_buy_qty != "" && $buy_avg_price != ""){
                if($buy_or_sell_db == "BUY"){
                    $open_or_close = "OPEN";
                }


                if($trade_type == "Cash"){
                    if($this->validateDate($series_or_expiry)){
                        if($strike_price != ""){
                            $sebi_turnover_fees = $this->brokerageCalculation(28, $turnover);
                            $calculate_cash[$i]["Sebi Turnover Fees"] = $sebi_turnover_fees;
                            
                            $stamp_duty_option = $this->brokerageCalculation(2, $turnover);
                            $calculate_cash[$i]["Stamp Duty Option"] = $stamp_duty_option;
                            
                            $security_transaction_option = 0;
                            
                            if($net_sell_qty != "" && $sell_avg_price != ""){
                                $security_transaction_option = $this->brokerageCalculation(25, $turnover_sell);
                                $calculate_cash[$i]["Security Transaction Option"] = $security_transaction_option;
                            }
                            
                            $summ_stt_sd = $stamp_duty_option+$security_transaction_option;
                            
                            if($type == "NSE"){
                                $transaction_charges = $this->brokerageCalculation(22, $turnover);
                                $calculate_cash[$i]["Transaction Charges NSE Option"] = $transaction_charges;
                            }
                            else if($type == "BSE"){
                                $transaction_charges = $this->brokerageCalculation(24, $turnover);
                                $calculate_cash[$i]["Transaction Charges BSE Option"] = $transaction_charges;
                            }
                            
                            if($net_buy_qty != "" && $buy_avg_price != "" && $net_sell_qty != "" && $sell_avg_price != ""){
                                $brokerage = $query_brokerage[0]->option_rate*2;
                            }
                            else{
                                $brokerage = $query_brokerage[0]->option_rate;
                            }
                            $calculate_cash[$i]["Option Brokerage"] = $brokerage;
                        }
                        else{
                            $sebi_turnover_fees = $this->brokerageCalculation(28, $turnover);
                            $calculate_cash[$i]["Sebi Turnover Fees"] = $sebi_turnover_fees;
                            
                            $security_transaction_future = 0;
                            $stamp_duty_future = 0;
                            
                            if($net_sell_qty != "" && $sell_avg_price != ""){
                                $security_transaction_future = $this->brokerageCalculation(27, $turnover_sell);
                                $calculate_cash[$i]["Security Transaction Future"] = $security_transaction_future;
                            }
                            
                            $summ_stt_sd = $stamp_duty_future+$security_transaction_future;
                            
                            if($type == "NSE"){
                                $transaction_charges = $this->brokerageCalculation(21, $turnover);
                                $calculate_cash[$i]["Transaction Charges NSE Future"] = $transaction_charges;
                            }
                            else if($type == "BSE"){
                                $transaction_charges = $this->brokerageCalculation(23, $turnover);
                                $calculate_cash[$i]["Transaction Charges BSE Future"] = $transaction_charges;
                            }
                            
                            $brokerage = ($turnover*$query_brokerage[0]->future_rate)/100;
                            $calculate_cash[$i]["Future Brokerage"] = $brokerage;
                        }
                    }
                    else if($net_buy_qty != "" && $buy_avg_price != "" && $net_sell_qty != "" && $sell_avg_price != ""){
                        if($type == "NSE"){
                            $transaction_charges = $this->brokerageCalculation(4, $turnover);
                            $calculate_cash[$i]["Transaction Charges NSE"] = $transaction_charges;
                        }
                        else if($type == "BSE"){
                            $transaction_charges = $this->brokerageCalculation(5, $turnover);
                            $calculate_cash[$i]["Transaction Charges BSE"] = $transaction_charges;
                        }
                        
                        $trade_on = "intraday";
                        
                        $stamp_duty_intraday = $this->brokerageCalculation(2, $turnover);
                        $calculate_cash[$i]["Stamp Duty Intraday"] = $stamp_duty_intraday;

                        $security_transaction_tax_intraday = $this->brokerageCalculation(16, $turnover_sell);
                        $calculate_cash[$i]["Security Transaction Tax Intraday"] = $security_transaction_tax_intraday;
                        
                        $summ_stt_sd = $stamp_duty_intraday+$security_transaction_tax_intraday;
                        
                        $brokerage = ($turnover*$query_brokerage[0]->cash_intraday_rate)/100;
                        $calculate_cash[$i]["Intraday Brokerage"] = $brokerage;
                        
                        $buy_brokerage = $brokerage;
                        $sell_brokerage = $brokerage;
                        
                        $sebi_turnover_fees = $this->brokerageCalculation(18, $turnover);
                        $calculate_cash[$i]["Sebi Turnover Fees"] = $sebi_turnover_fees;
                    }
                    else if(($net_buy_qty != "" && $buy_avg_price != "") || ($net_sell_qty != "" && $sell_avg_price != "")){
                        
                        if($type == "NSE"){
                            $transaction_charges = $this->brokerageCalculation(4, $turnover);
                            $calculate_cash[$i]["Transaction Charges NSE"] = $transaction_charges;
                        }
                        else if($type == "BSE"){
                            $transaction_charges = $this->brokerageCalculation(5, $turnover);
                            $calculate_cash[$i]["Transaction Charges BSE"] = $transaction_charges;
                        }
                        
                        $trade_on = "delivery";
                        $stamp_duty_delivery = $this->brokerageCalculation(3, $turnover);
                        $calculate_cash[$i]["Stamp Duty Delivery"] = $stamp_duty_delivery;

                        $security_transaction_tax_delivery = $this->brokerageCalculation(15, $turnover);
                        $calculate_cash[$i]["Security Transaction Tax Delivery"] = $security_transaction_tax_delivery;
                        
                        $summ_stt_sd = $stamp_duty_delivery+$security_transaction_tax_delivery;
                        
                        $brokerage = ($turnover*$query_brokerage[0]->cash_delivery_rate)/100;
                        $calculate_cash[$i]["Delivery Brokerage"] = $brokerage;
                        
                        $sebi_turnover_fees = $this->brokerageCalculation(18, $turnover);
                        $calculate_cash[$i]["Sebi Turnover Fees"] = $sebi_turnover_fees;
                        if($net_buy_qty != "" && $buy_avg_price != "")
                            $buy_brokerage = $brokerage;
                        if($net_sell_qty != "" && $sell_avg_price != "")
                            $sell_brokerage = $brokerage;
                        //$calculate_cash["validate date"] = $series_or_expiry;
                    }
                    
                    //$brokerage = 
                    
                    if($trade_on == "delivery" && $net_sell_qty != "" && $sell_avg_price != ""){
                        $exit_load = 15+($this->brokerageCalculation(74, 15));
                        $calculate_cash[$i]["DP Exit Load"] = $exit_load;
                    }
                    
                    $gst = $this->brokerageCalculation(1, ($brokerage+$transaction_charges+$sebi_turnover_fees));
                    $calculate_cash[$i]["GST"] = $gst;
                }
                else if($trade_on == "Currency"){
                    if(validateDate($series_or_expiry)){
                        if($strike_price != ""){
                            $sebi_turnover_fees = $this->brokerageCalculation(50, $turnover);
                            $calculate_cash[$i]["Sebi Turnover Fees"] = $sebi_turnover_fees;
                            
                            // if($nse_bse == "NSE"){
                                
                            // }
                            // else if($nse_bse == "BSE"){

                            // }
                            
                            
                            $transaction_charges = $this->brokerageCalculation(32, $turnover);
                            $calculate_cash[$i]["Transaction Charges NSE Option"] = $transaction_charges;
                            
                            if($net_buy_qty != "" && $buy_avg_price != "" && $net_sell_qty != "" && $sell_avg_price != ""){
                                $brokerage = $query_brokerage[0]->currency_option_rate*2;
                            }
                            else{
                                $brokerage = $query_brokerage[0]->currency_option_rate;
                            }
                            $calculate_cash[$i]["Option Brokerage"] = $brokerage;
                            
                            $gst = $this->brokerageCalculation(1, ($brokerage+$transaction_charges+$sebi_turnover_fees));
                            $calculate_cash[$i]["GST"] = $gst;
                            
                            $stamp_duty_option = $this->brokerageCalculation(47, $turnover);
                            $calculate_cash[$i]["Stamp Duty Option"] = $stamp_duty_option;
                        }
                        else{
                            $sebi_turnover_fees = $this->brokerageCalculation(48, $turnover);
                            $calculate_cash[$i]["Sebi Turnover Fees"] = $sebi_turnover_fees;
                            
                            $transaction_charges = $this->brokerageCalculation(30, $turnover);
                            $calculate_cash[$i]["Transaction Charges Future"] = $transaction_charges;
                            
                            $brokerage = ($turnover*$query_brokerage[0]->currency_future_rate)/100;
                            $calculate_cash[$i]["Future Brokerage"] = $brokerage;
                            
                            $gst = $this->brokerageCalculation(1, ($brokerage+$transaction_charges+$sebi_turnover_fees));
                            $calculate_cash[$i]["GST"] = $gst;
                        }
                    }
                }
                else if($trade_type == "Commodities"){
                    if($type == "MCX"){
                        $sebi_turnover_fees = $this->brokerageCalculation(63, $turnover);
                        $calculate_cash[$i]["Sebi Turnover Fees"] = $sebi_turnover_fees;
                        
                        $security_transaction_future = 0;
                        $stamp_duty_future = 0;

                        if($net_sell_qty != "" && $sell_avg_price != ""){
                            $security_transaction_future = $this->brokerageCalculation(54, $turnover_sell);
                            $calculate_cash[$i]["Commodities Transaction Future"] = $security_transaction_future;
                        }
                        
                        $stamp_duty_future = $this->brokerageCalculation(53, $turnover);
                        $calculate_cash[$i]["Stamp Duty Future"] = $stamp_duty_future;

                        $summ_stt_sd = $stamp_duty_future+$security_transaction_future;
                        
                        $transaction_charges = $this->brokerageCalculation(55, $turnover);
                        $calculate_cash[$i]["Transaction Charges Future"] = $transaction_charges;
                        
                        $brokerage = ($turnover*$query_brokerage[0]->commodities)/100;
                        $calculate_cash[$i]["Future Brokerage"] = $brokerage;
                        
                        $gst = $this->brokerageCalculation(1, ($brokerage+$transaction_charges+$sebi_turnover_fees));
                        $calculate_cash[$i]["GST"] = $gst;
                    }
                    else if($type == "NCDEX"){
                        $sebi_turnover_fees = $this->brokerageCalculation(63, $turnover);
                        $calculate_cash[$i]["Sebi Turnover Fees"] = $sebi_turnover_fees;
                        
                        $risk_management_fee = $this->brokerageCalculation(62, $turnover);
                        $calculate_cash[$i]["Risk Management Fee"] = $risk_management_fee;
                        
                        $security_transaction_future = 0;
                        $stamp_duty_future = 0;
                        
                        if($net_sell_qty != "" && $sell_avg_price != ""){
                            $security_transaction_future = $this->brokerageCalculation(60, $turnover_sell);
                            $calculate_cash[$i]["Commodities Transaction Future"] = $security_transaction_future;
                        }
                        
                        $stamp_duty_future = $this->brokerageCalculation(53, $turnover);
                        $calculate_cash[$i]["Stamp Duty Future"] = $stamp_duty_future;

                        $summ_stt_sd = $stamp_duty_future+$security_transaction_future;
                        
                        $transaction_charges = $this->brokerageCalculation(57, $turnover);
                        $calculate_cash[$i]["Transaction Charges Future"] = $transaction_charges;
                        
                        $brokerage = ($turnover*$query_brokerage[0]->commodities)/100;
                        $calculate_cash[$i]["Future Brokerage"] = $brokerage;
                        
                        $gst = $this->brokerageCalculation(1, ($brokerage+$transaction_charges+$sebi_turnover_fees));
                        $calculate_cash[$i]["GST"] = $gst;
                    }
                }
                
                $total_brokerage = $total_brokerage+$brokerage;
                
                $total_taxes = $transaction_charges+$gst+$summ_stt_sd+$sebi_turnover_fees+$brokerage+$exit_load;
                $calculate_cash[$i]["Total Expenses"] = $total_taxes;
                $brokerage_old_arr[$i]["Brokerage Old"] = $brokerage;
                $i++;

                $margin = ($net_buy_qty*$buy_avg_price)+$brokerage;
                $profit_or_loss = ($sell_rate-$purchase_rate)*$total_qty-($brokerage);

                if($buy_or_sell == "BUY" || $buy_or_sell == "BUY/SELL"){
                    
                    $transaction = Transactions::create([

                        'user_id'=>auth()->id(),
                        'account_id'=>$result->account_id,
                        'segment'=>$trade_type,
                        'buy_date'=>$buy_date,
                        'buy_or_sell'=>$buy_or_sell,
                        'qty'=>$total_qty,
                        'script_name'=>$symbol,
                        'purchase_rate'=>$buy_avg_price,
                        'purchase_tax'=>$buy_brokerage,
                        'sell_date'=>$sell_date, 
                        'sell_rate'=>$sell_avg_price,
                        'sell_tax'=>$sell_brokerage,
                        'open_or_close'=>$open_or_close,
                        'sl_or_trail'=>'',
                        'target'=>'',
                        'margin'=>$margin,
                        'profit_or_loss'=>$profit_or_loss,
                        'timeframe'=>$timeframe,
                        'created_ip'=>\Request::ip(),
                        'created_at'=>Carbon::now()
                    ]);

                    if($transaction){
                        $deals = Deals::find($result->id);

                        $deals->status = 1;

                        $deals->save();

                        $response = "Data uploaded successfully.";
                    }
                    else{
                        $response = "An error occured.";
                    }
                }
                else if($buy_or_sell == "SELL"){
                    
                }

            }
        }

        return redirect('/deals');

        //dd($response);
    }

    public function validateDate($date){
        $d = \DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') == $date;
    }

    public function brokerageCalculation($id, $amount){
        
        $query = \DB::select("SELECT bc.name, bw.brokerage_id, bw.rate, bw.wef FROM brokerage_wef bw, brokerage_charges bc WHERE (bc.id=bw.brokerage_id) AND (bw.brokerage_id=".$id.") AND (bw.wef <= '".date("Y-m-d")."') order by bw.wef desc limit 1");

        if(!empty($query))
            return $query[0]->rate;
        else
            return 1;
    }


    public function getData(){

        $dealsById = Deals::find(request('id'));

        $getDeals = \DB::select("SELECT * FROM transactions WHERE account_id='".$dealsById->account_id."' and script_name='".$dealsById->symbol."' and open_or_close='OPEN' and buy_date<='".$dealsById->deal_date."' order by purchase_rate asc");

        return \Response::json($getDeals);
    }

    public function calcSell(){

        $a = array();
        $calculate_cash = array();

        $response = "";
        $deal_date = "";
        $sell_date = request('sellDate');
        
        $purchase_id_comma = request('purchaseId');
        $purchase_id = explode(",", $purchase_id_comma);

        $sell_quantity_comma = request('sellQuantity');
        $sell_quantity = explode(",", $sell_quantity_comma);

        $sellID = request('id');

        $brokerage_old_arr = array();
        $brokerage_new_arr = array();

        $transaction_charges = 0;

        $total_brokerage = 0;

        $response = "Error";
        
        $value = array();

        $i = 0;

        $queryDeals = Deals::where('id','=',$sellID)->where('status','=',0)->get();

        foreach ($queryDeals as $queryDealsValue) {
            
            $calculate_cash[$i]["ID"] = $queryDealsValue->id;
            $userid = $queryDealsValue->account_id;
            $type = $queryDealsValue->exchange_segment;
            $symbol = $queryDealsValue->symbol;
            $series_or_expiry = $queryDealsValue->expiry_date;
            $strike_price = $queryDealsValue->strike_price;
            $option_type = $queryDealsValue->option_type;
            $trade_qty = $queryDealsValue->trade_qty;
            $trade_price = $queryDealsValue->trade_price;
            $buy_or_sell_db = $queryDealsValue->buy_or_sell;
            $deal_date = $queryDealsValue->deal_date;


            $calculated_value = $this->getSellCalculated($calculate_cash, $userid, $type, $symbol, $series_or_expiry, $strike_price, $option_type, $trade_qty, $trade_price, $buy_or_sell_db, $deal_date);
            //$response = "Deals if". $queryDealsValue->buy_or_sell;

            if($buy_or_sell_db == "SELL"){
                $countRow = count($purchase_id);
                $response = $countRow;
                if($countRow > 0){
                    for($i=0; $i<count($purchase_id); $i++){
                        $timeframe = "M";
                        $queryDealsBuyId = \DB::select("SELECT * FROM transactions WHERE id='".$purchase_id[$i]."'");
                        $count2 = count($queryDealsBuyId);
                        if(!empty($queryDealsBuyId)){
                            $quantity_after_substraction = $queryDealsBuyId[0]->qty-$sell_quantity[$i];
                            if($queryDealsBuyId[0]->buy_date == $sell_date){
                                $timeframe = "I";
                            }

                            //$updateTransaction = Transactions::where('id', '=', $purchase_id[$i])->get();
                            $updateTransaction = Transactions::find($purchase_id[$i]);

                            $updateTransaction->sell_date = $sell_date;
                            $updateTransaction->sell_tax = $calculated_value;
                            $updateTransaction->open_or_close = "CLOSE";
                            $updateTransaction->timeframe = $timeframe;

                            $updateTransaction->save();

                            $response = "Data updated successfully.";

                            //$updateDealById = Deals::where('id','=',$sellID);
                            $updateDealById = Deals::find($sellID);

                            $updateDealById->status = 1;

                            $updateDealById->save();
                        }
                        $response = $queryDealsBuyId[0]->qty;
                        if($quantity_after_substraction != 0){
                            $brokerage_on_qty_sold = $this->getSellCalculated($calculate_cash, $userid, $type, $symbol, $series_or_expiry, $strike_price, $option_type, $sell_quantity[$i], $queryDealsBuyId[0]->purchase_rate, $buy_or_sell_db, $deal_date);

                            $update_brokerage_on_qty_sold = Transactions::find($purchase_id[$i]);

                            $update_brokerage_on_qty_sold->sell_tax = $brokerage_on_qty_sold;
                            $update_brokerage_on_qty_sold->qty = $sell_quantity[$i];

                            $update_brokerage_on_qty_sold->save();

                            $brokerage_on_quantity_remaining = $this->getSellCalculated($calculate_cash, $userid, $type, $symbol, $series_or_expiry, $strike_price, $option_type, $quantity_after_substraction, $queryDealsBuyId[0]->purchase_rate, $buy_or_sell_db, $deal_date);

                            $query_insert_buy_by_id = Transactions::create([

                                'user_id'=>auth()->id(),
                                'account_id'=>$queryDealsBuyId[0]->account_id,
                                'segment'=>$queryDealsBuyId[0]->segment,
                                'buy_date'=>$queryDealsBuyId[0]->buy_date,
                                'buy_or_sell'=>$queryDealsBuyId[0]->buy_or_sell,
                                'qty'=>$quantity_after_substraction,
                                'script_name'=>$queryDealsBuyId[0]->script_name,
                                'purchase_rate'=>$queryDealsBuyId[0]->purchase_rate,
                                'purchase_tax'=>$queryDealsBuyId[0]->purchase_tax,
                                'sell_date'=>$queryDealsBuyId[0]->sell_date, 
                                'sell_rate'=>$queryDealsBuyId[0]->sell_rate,
                                'sell_tax'=>$brokerage_on_quantity_remaining,
                                'open_or_close'=>$queryDealsBuyId[0]->open_or_close,
                                'sl_or_trail'=>$queryDealsBuyId[0]->sl_or_trail,
                                'target'=>$queryDealsBuyId[0]->target,
                                'margin'=>$queryDealsBuyId[0]->margin,
                                'profit_or_loss'=>$queryDealsBuyId[0]->profit_or_loss,
                                'timeframe'=>$queryDealsBuyId[0]->timeframe,
                                'created_ip'=>\Request::ip(),
                                'created_at'=>Carbon::now()

                            ]);

                            if($query_insert_buy_by_id){
                                $response = "Data uploaded successfully!!";
                            }
                        }
                    }
                }
            }
        }
        return \Response::json($response);

    }

    public function getSellCalculated($calculate_cash, $userid, $type, $symbol, $series_or_expiry, $strike_price, $option_type, $trade_qty, $trade_price, $buy_or_sell_db, $deal_date){
        $i = 0;
        $avg_price = $trade_price;


                $net_buy_qty = $trade_qty;
                $buy_avg_price = $avg_price;
                $net_sell_qty = $trade_qty;
                $sell_avg_price = $avg_price;
                
                $turnover = 0;
                $stamp_duty_intraday = 0;
                $stamp_duty_delivery = 0;
                $transaction_charges_nse = 0;
                $transaction_charges_bse = 0;
                $security_transaction_tax_delivery = 0;
                $security_transaction_tax_intraday = 0;
                $sebi_turnover_fees = 0;
                $gst = 0;
                $buy_sale_amount = 0;
                $brokerage = 0;
                $exit_load = 0;
                $total_taxes = 0;
                $turnover_buy = 0;
                $turnover_sell = 0;
                $summ_stt_sd = 0;
                $stamp_duty_commodities = 0;
                $risk_management_fee = 0;
                $total_qty = 0;
                $margin = 0;
                $sell_rate = 0;
                $purchase_rate = 0;
                $profit_or_loss = 0;

                $total_brokerage = 0;
                
                
                //$tot_brokerage = 0;
                $buy_or_sell = "";
                $open_or_close = "";
                $trade_type = "";
                $trade_on = "";
                $nse_bse = "";
                $buy_date = "";
                $sell_date = "";
                $timeframe = "M";
                $buy_brokerage = 0;
                $sell_brokerage = 0;
                
                //if($net_buy_qty != "" && $buy_avg_price != ""){
                if($buy_or_sell_db == "BUY"){
                    $turnover = $net_buy_qty*$buy_avg_price;
                    $turnover_buy = $turnover;
                    //$calculate_cash["Turnover buy"] = $turnover_buy;
                    $buy_sale_amount = $buy_avg_price;
                    $buy_or_sell = "BUY";
                    $buy_date = $deal_date;
                    $total_qty = $net_buy_qty;
                }
                //if($net_sell_qty != "" && $sell_avg_price != ""){
                if($buy_or_sell_db == "SELL"){
                    $turnover = $net_sell_qty*$sell_avg_price;
                    $turnover_sell = $turnover;
                    $buy_sale_amount = $sell_avg_price;
                    $buy_or_sell = "SELL";
                    $sell_date = $deal_date;
                    $total_qty = $net_sell_qty;
                }
                
                //if($net_buy_qty != "" && $buy_avg_price != "" && $net_sell_qty != "" && $sell_avg_price != ""){
                if($buy_or_sell_db == "BUY" && $buy_or_sell_db == "SELL"){
                    $turnover = $turnover_buy+$turnover_sell;
                    //$calculate_cash["Turnover buy+Sell"] = $turnover;
                    $buy_or_sell = "BUY/SELL";
                    $buy_date = $deal_date;
                    $sell_date = $deal_date;
                    $timeframe = "I";
                }
                
                $turn_over = $avg_price*$trade_qty;

                $query_brokerage = ClientBrokerage::limit(1)->where('account_id','=',$userid)->where('wef','<=',Carbon::now()->format('Y-m-d'))->get();

                //$this->brokerageCalculation(1, 120000);

                //dd($query_brokerage[0]);
                //dd($query_brokerage[0]->id);

                if($type == "NSE" || $type == "BSE" || $type == "NFO"){
                    $trade_type = "Cash";
                }
                else if($type == "NSE-CDS" || $type == "BSE-CDS"){
                    $trade_on = "Currency";
                    if($type == "NSE-CDS")
                        $nse_bse = "NSE";
                    else if($type == "BSE-CDS")
                        $nse_bse = "BSE";
                }
                else if($type == "MCX" || $type == "NCDEX"){
                    $trade_type = "Commodities";
                }
                $open_or_close = "";
                //if($net_sell_qty != "" && $sell_avg_price != ""){
                if($buy_or_sell_db == "SELL"){
                    $open_or_close = "CLOSE";
                }
                //if($net_buy_qty != "" && $buy_avg_price != ""){
                if($buy_or_sell_db == "BUY"){
                    $open_or_close = "OPEN";
                }


                if($trade_type == "Cash"){
                    if($this->validateDate($series_or_expiry)){
                        if($strike_price != ""){
                            $sebi_turnover_fees = $this->brokerageCalculation(28, $turnover);
                            $calculate_cash[$i]["Sebi Turnover Fees"] = $sebi_turnover_fees;
                            
                            $stamp_duty_option = $this->brokerageCalculation(2, $turnover);
                            $calculate_cash[$i]["Stamp Duty Option"] = $stamp_duty_option;
                            
                            $security_transaction_option = 0;
                            
                            if($net_sell_qty != "" && $sell_avg_price != ""){
                                $security_transaction_option = $this->brokerageCalculation(25, $turnover_sell);
                                $calculate_cash[$i]["Security Transaction Option"] = $security_transaction_option;
                            }
                            
                            $summ_stt_sd = $stamp_duty_option+$security_transaction_option;
                            
                            if($type == "NSE"){
                                $transaction_charges = $this->brokerageCalculation(22, $turnover);
                                $calculate_cash[$i]["Transaction Charges NSE Option"] = $transaction_charges;
                            }
                            else if($type == "BSE"){
                                $transaction_charges = $this->brokerageCalculation(24, $turnover);
                                $calculate_cash[$i]["Transaction Charges BSE Option"] = $transaction_charges;
                            }
                            
                            if($net_buy_qty != "" && $buy_avg_price != "" && $net_sell_qty != "" && $sell_avg_price != ""){
                                $brokerage = $query_brokerage[0]->option_rate*2;
                            }
                            else{
                                $brokerage = $query_brokerage[0]->option_rate;
                            }
                            $calculate_cash[$i]["Option Brokerage"] = $brokerage;
                        }
                        else{
                            $sebi_turnover_fees = $this->brokerageCalculation(28, $turnover);
                            $calculate_cash[$i]["Sebi Turnover Fees"] = $sebi_turnover_fees;
                            
                            $security_transaction_future = 0;
                            $stamp_duty_future = 0;
                            
                            if($net_sell_qty != "" && $sell_avg_price != ""){
                                $security_transaction_future = $this->brokerageCalculation(27, $turnover_sell);
                                $calculate_cash[$i]["Security Transaction Future"] = $security_transaction_future;
                            }
                            
                            $summ_stt_sd = $stamp_duty_future+$security_transaction_future;
                            
                            if($type == "NSE"){
                                $transaction_charges = $this->brokerageCalculation(21, $turnover);
                                $calculate_cash[$i]["Transaction Charges NSE Future"] = $transaction_charges;
                            }
                            else if($type == "BSE"){
                                $transaction_charges = $this->brokerageCalculation(23, $turnover);
                                $calculate_cash[$i]["Transaction Charges BSE Future"] = $transaction_charges;
                            }
                            
                            $brokerage = ($turnover*$query_brokerage[0]->future_rate)/100;
                            $calculate_cash[$i]["Future Brokerage"] = $brokerage;
                        }
                    }
                    else if($net_buy_qty != "" && $buy_avg_price != "" && $net_sell_qty != "" && $sell_avg_price != ""){
                        if($type == "NSE"){
                            $transaction_charges = $this->brokerageCalculation(4, $turnover);
                            $calculate_cash[$i]["Transaction Charges NSE"] = $transaction_charges;
                        }
                        else if($type == "BSE"){
                            $transaction_charges = $this->brokerageCalculation(5, $turnover);
                            $calculate_cash[$i]["Transaction Charges BSE"] = $transaction_charges;
                        }
                        
                        $trade_on = "intraday";
                        
                        $stamp_duty_intraday = $this->brokerageCalculation(2, $turnover);
                        $calculate_cash[$i]["Stamp Duty Intraday"] = $stamp_duty_intraday;

                        $security_transaction_tax_intraday = $this->brokerageCalculation(16, $turnover_sell);
                        $calculate_cash[$i]["Security Transaction Tax Intraday"] = $security_transaction_tax_intraday;
                        
                        $summ_stt_sd = $stamp_duty_intraday+$security_transaction_tax_intraday;
                        
                        $brokerage = ($turnover*$query_brokerage[0]->cash_intraday_rate)/100;
                        $calculate_cash[$i]["Intraday Brokerage"] = $brokerage;
                        
                        $buy_brokerage = $brokerage;
                        $sell_brokerage = $brokerage;
                        
                        $sebi_turnover_fees = $this->brokerageCalculation(18, $turnover);
                        $calculate_cash[$i]["Sebi Turnover Fees"] = $sebi_turnover_fees;
                    }
                    else if(($net_buy_qty != "" && $buy_avg_price != "") || ($net_sell_qty != "" && $sell_avg_price != "")){
                        
                        if($type == "NSE"){
                            $transaction_charges = $this->brokerageCalculation(4, $turnover);
                            $calculate_cash[$i]["Transaction Charges NSE"] = $transaction_charges;
                        }
                        else if($type == "BSE"){
                            $transaction_charges = $this->brokerageCalculation(5, $turnover);
                            $calculate_cash[$i]["Transaction Charges BSE"] = $transaction_charges;
                        }
                        
                        $trade_on = "delivery";
                        $stamp_duty_delivery = $this->brokerageCalculation(3, $turnover);
                        $calculate_cash[$i]["Stamp Duty Delivery"] = $stamp_duty_delivery;

                        $security_transaction_tax_delivery = $this->brokerageCalculation(15, $turnover);
                        $calculate_cash[$i]["Security Transaction Tax Delivery"] = $security_transaction_tax_delivery;
                        
                        $summ_stt_sd = $stamp_duty_delivery+$security_transaction_tax_delivery;
                        
                        $brokerage = ($turnover*$query_brokerage[0]->cash_delivery_rate)/100;
                        $calculate_cash[$i]["Delivery Brokerage"] = $brokerage;
                        
                        $sebi_turnover_fees = $this->brokerageCalculation(18, $turnover);
                        $calculate_cash[$i]["Sebi Turnover Fees"] = $sebi_turnover_fees;
                        if($net_buy_qty != "" && $buy_avg_price != "")
                            $buy_brokerage = $brokerage;
                        if($net_sell_qty != "" && $sell_avg_price != "")
                            $sell_brokerage = $brokerage;
                        //$calculate_cash["validate date"] = $series_or_expiry;
                    }
                    
                    //$brokerage = 
                    
                    if($trade_on == "delivery" && $net_sell_qty != "" && $sell_avg_price != ""){
                        $exit_load = 15+($this->brokerageCalculation(74, 15));
                        $calculate_cash[$i]["DP Exit Load"] = $exit_load;
                    }
                    
                    $gst = $this->brokerageCalculation(1, ($brokerage+$transaction_charges+$sebi_turnover_fees));
                    $calculate_cash[$i]["GST"] = $gst;
                }
                else if($trade_on == "Currency"){
                    if(validateDate($series_or_expiry)){
                        if($strike_price != ""){
                            $sebi_turnover_fees = $this->brokerageCalculation(50, $turnover);
                            $calculate_cash[$i]["Sebi Turnover Fees"] = $sebi_turnover_fees;
                            
                            // if($nse_bse == "NSE"){
                                
                            // }
                            // else if($nse_bse == "BSE"){

                            // }
                            
                            
                            $transaction_charges = $this->brokerageCalculation(32, $turnover);
                            $calculate_cash[$i]["Transaction Charges NSE Option"] = $transaction_charges;
                            
                            if($net_buy_qty != "" && $buy_avg_price != "" && $net_sell_qty != "" && $sell_avg_price != ""){
                                $brokerage = $query_brokerage[0]->currency_option_rate*2;
                            }
                            else{
                                $brokerage = $query_brokerage[0]->currency_option_rate;
                            }
                            $calculate_cash[$i]["Option Brokerage"] = $brokerage;
                            
                            $gst = $this->brokerageCalculation(1, ($brokerage+$transaction_charges+$sebi_turnover_fees));
                            $calculate_cash[$i]["GST"] = $gst;
                            
                            $stamp_duty_option = $this->brokerageCalculation(47, $turnover);
                            $calculate_cash[$i]["Stamp Duty Option"] = $stamp_duty_option;
                        }
                        else{
                            $sebi_turnover_fees = $this->brokerageCalculation(48, $turnover);
                            $calculate_cash[$i]["Sebi Turnover Fees"] = $sebi_turnover_fees;
                            
                            $transaction_charges = $this->brokerageCalculation(30, $turnover);
                            $calculate_cash[$i]["Transaction Charges Future"] = $transaction_charges;
                            
                            $brokerage = ($turnover*$query_brokerage[0]->currency_future_rate)/100;
                            $calculate_cash[$i]["Future Brokerage"] = $brokerage;
                            
                            $gst = $this->brokerageCalculation(1, ($brokerage+$transaction_charges+$sebi_turnover_fees));
                            $calculate_cash[$i]["GST"] = $gst;
                        }
                    }
                }
                else if($trade_type == "Commodities"){
                    if($type == "MCX"){
                        $sebi_turnover_fees = $this->brokerageCalculation(63, $turnover);
                        $calculate_cash[$i]["Sebi Turnover Fees"] = $sebi_turnover_fees;
                        
                        $security_transaction_future = 0;
                        $stamp_duty_future = 0;

                        if($net_sell_qty != "" && $sell_avg_price != ""){
                            $security_transaction_future = $this->brokerageCalculation(54, $turnover_sell);
                            $calculate_cash[$i]["Commodities Transaction Future"] = $security_transaction_future;
                        }
                        
                        $stamp_duty_future = $this->brokerageCalculation(53, $turnover);
                        $calculate_cash[$i]["Stamp Duty Future"] = $stamp_duty_future;

                        $summ_stt_sd = $stamp_duty_future+$security_transaction_future;
                        
                        $transaction_charges = $this->brokerageCalculation(55, $turnover);
                        $calculate_cash[$i]["Transaction Charges Future"] = $transaction_charges;
                        
                        $brokerage = ($turnover*$query_brokerage[0]->commodities)/100;
                        $calculate_cash[$i]["Future Brokerage"] = $brokerage;
                        
                        $gst = $this->brokerageCalculation(1, ($brokerage+$transaction_charges+$sebi_turnover_fees));
                        $calculate_cash[$i]["GST"] = $gst;
                    }
                    else if($type == "NCDEX"){
                        $sebi_turnover_fees = $this->brokerageCalculation(63, $turnover);
                        $calculate_cash[$i]["Sebi Turnover Fees"] = $sebi_turnover_fees;
                        
                        $risk_management_fee = $this->brokerageCalculation(62, $turnover);
                        $calculate_cash[$i]["Risk Management Fee"] = $risk_management_fee;
                        
                        $security_transaction_future = 0;
                        $stamp_duty_future = 0;
                        
                        if($net_sell_qty != "" && $sell_avg_price != ""){
                            $security_transaction_future = $this->brokerageCalculation(60, $turnover_sell);
                            $calculate_cash[$i]["Commodities Transaction Future"] = $security_transaction_future;
                        }
                        
                        $stamp_duty_future = $this->brokerageCalculation(53, $turnover);
                        $calculate_cash[$i]["Stamp Duty Future"] = $stamp_duty_future;

                        $summ_stt_sd = $stamp_duty_future+$security_transaction_future;
                        
                        $transaction_charges = $this->brokerageCalculation(57, $turnover);
                        $calculate_cash[$i]["Transaction Charges Future"] = $transaction_charges;
                        
                        $brokerage = ($turnover*$query_brokerage[0]->commodities)/100;
                        $calculate_cash[$i]["Future Brokerage"] = $brokerage;
                        
                        $gst = $this->brokerageCalculation(1, ($brokerage+$transaction_charges+$sebi_turnover_fees));
                        $calculate_cash[$i]["GST"] = $gst;
                    }
                }
                
                $total_brokerage = $total_brokerage+$brokerage;
                
                $total_taxes = $transaction_charges+$gst+$summ_stt_sd+$sebi_turnover_fees+$brokerage+$exit_load;
                $calculate_cash[$i]["Total Expenses"] = $total_taxes;
                $brokerage_old_arr[$i]["Brokerage Old"] = $brokerage;

                $i++;

                $margin = ($net_buy_qty*$buy_avg_price)+$brokerage;
                $profit_or_loss = ($sell_rate-$purchase_rate)*$total_qty-($brokerage);

                return $brokerage;

    }

    public function viewCalc(){
        return view('deals.calculate');
    }

}
