<?php

namespace App\Http\Controllers;

//use App\Http\Request;


//use Request;
use Illuminate\Http\Request;
use App\Clients;
use Excel;
use Carbon\Carbon;

class ClientsController extends Controller
{
    public function __construct(){

    	$this->middleware('auth');

    }

    public function view(Request $request){

        $clients = Clients::paginate(3);

        return view('clients.view', compact('clients'))->with('i', ($request->input('page', 1)-1)*3);

    	//$clients = Clients::all();
    	//dd($clients);
    	//return view('clients.view', compact('clients'));


    }

    public function add(){
    	return view('clients.add');
    }

    public function save(){

    	$this->validate(request(),[
    		'inputClientId'=>'required',
    		'inputAccountId'=>'required',
    		'inputName'=>'required',
    		'inputNumber'=>'required',
    		'inputEmail'=>'required',
    		'inputCity'=>'required',
    		'inputInvestmentAmount'=>'required',
    		'inputInvestmentDate'=>'required'
    	]);

    	Clients::create([
    		'client_id'=>auth()->id(),
    		'account_id'=>request('inputAccountId'),
    		'name'=>request('inputName'),
    		'number'=>request('inputNumber'),
    		'email'=>request('inputEmail'),
    		'city'=>request('inputCity'),
    		'investment_amount'=>request('inputInvestmentAmount'),
    		'investment_date'=>request('inputInvestmentDate'),
    		'created_ip'=>\Request::ip(),
    		'modified_ip'=>\Request::ip()
    	]);

    	session()->flash('alert-success', 'Data save successfully!');

    	return redirect('/clients/add');

    }


    public function showSingle($id){

    	$clients = Clients::find($id);

    	return view('/clients/edit', compact('clients'));

    }

    public function updateSingle($id){

    	$this->validate(request(),[
    		'inputClientId'=>'required',
    		'inputAccountId'=>'required',
    		'inputName'=>'required',
    		'inputNumber'=>'required',
    		'inputEmail'=>'required',
    		'inputCity'=>'required',
    		'inputInvestmentAmount'=>'required',
    		'inputInvestmentDate'=>'required'
    	]);

    	$client = Clients::find($id);

    	$client->name = request('inputName');
    	$client->number = request('inputNumber');
    	$client->email = request('inputEmail');
    	$client->city = request('inputCity');
    	$client->investment_amount = request('inputInvestmentAmount');
    	$client->investment_date = request('inputInvestmentDate');
    	$client->modified_ip = \Request::ip();
        $client->updated_at = Carbon::now()->toDateTimeString();
    	$client->save();

    	session()->flash('alert-success', 'Data updated successfully!');

    	return redirect('/clients/edit/'.$id);
    }

    public function ExcelUpload(Request $request){
    	//dd($request->all());
    	if($request->hasFile('excelFile')){
            //dd($request->input('excelFile'));
    		$path = $request->file('excelFile')->getRealPath();

    		$data = Excel::load($path, function($reader){})->get();
    		if(!empty($data) && $data->count()){

    			foreach($data as $key => $value){
                    $insert[] = ['client_id'=>auth()->id(), 'account_id' => $value->account_id, 'name' => $value->name, 'number'=>$value->number, 'email'=>$value->email, 'city'=>$value->city, 'investment_amount'=>$value->investment_amount, 'investment_date'=>$value->investment_date, 'created_ip'=>\Request::ip(), 'modified_ip'=>\Request::ip()];
    			}

                //dd($value);

    			if(!empty($insert)){
                    $insertData = Clients::insert($insert);
                    if ($insertData) {
                        session()->flash('success', 'Your Data has successfully imported');
                    }else {                        
                        session()->flash('danger', 'Error inserting the data..');
                        return back();
                    }
    			}

    		}

    	}

        //dd("ENds");
        session()->flash('alert-danger', 'Data updated successfully!');

        //return redirect('/clients');
    	return back();
    }

    public function delete($id){
        Clients::find($id)->delete();

        session()->flash('alert-success', 'Data deleted successfully!');

        return redirect('/clients');
    }

}
