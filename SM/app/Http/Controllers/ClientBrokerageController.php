<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClientBrokerage;

use Excel;

class ClientBrokerageController extends Controller
{
    public function __construct(){

    	return $this->middleware('auth');

    }

    public function index(Request $request){

    	$clientsBrokerage = ClientBrokerage::paginate(5);

    	return view('clients.brokerage.view', compact('clientsBrokerage'))->with('i', ($request->input('page', 1)-1)*5);

    }


    public function add(){

    	return view('clients.brokerage.add');

    }

    public function save(){

    	$this->validate(request(),[

    		'inputAccountId'=>'required',
    		'inputCashDeliveryRate'=>'required',
    		'inputCashIntradayRate'=>'required',
    		'inputFutureRate'=>'required',
    		'inputOptionRate'=>'required',
    		'inputCurrencyFutureRate'=>'required',
    		'inputCurrencyOptionRate'=>'required',
    		'inputCommodities'=>'required',
    		'inputWithEffectsFrom'=>'required'

    	]);

    	ClientBrokerage::create([
    		'client_id'=>auth()->id(),
    		'account_id'=>request('inputAccountId'),
    		'cash_delivery_rate'=>request('inputCashDeliveryRate'),
    		'cash_intraday_rate'=>request('inputCashIntradayRate'),
    		'future_rate'=>request('inputFutureRate'),
    		'option_rate'=>request('inputOptionRate'),
    		'currency_future_rate'=>request('inputCurrencyFutureRate'),
    		'currency_option_rate'=>request('inputCurrencyOptionRate'),
    		'commodities'=>request('inputCommodities'),
    		'wef'=>request('inputWithEffectsFrom'),
    		'created_ip'=>\Request::ip()
    	]);

    	session()->flash('alert-success', 'Data saved successfully!');

    	return redirect('/clients/brokerage/add');

    }


    public function ExcelUpload(Request $request){

    	if($request->hasFile('excelFile')){

    		$path = $request->file('excelFile')->getRealPath();

    		$data = Excel::load($path, function($reader){})->get();

    		if(!empty($data) && $data->count()){
                //dd($data);
    			foreach($data as $key => $value){

    				$insert[] = ['client_id'=>auth()->id(), 'account_id'=>$value->account_id, 'cash_delivery_rate'=>$value->cash_delivery_rate, 'cash_intraday_rate'=>$value->cash_intraday_rate, 'future_rate'=>$value->future_rate, 'option_rate'=>$value->option_rate, 'currency_future_rate'=>$value->currency_future_rate, 'currency_option_rate'=>$value->currency_option_rate, 'commodities'=>$value->commodities, 'wef'=>$value->wef, 'created_ip'=>\Request::ip()];

    			}
                //dd($insert);
    			if(!empty($insert)){

    				$insertCB = ClientBrokerage::insert($insert);

    				if($insertCB){
    					session()->flash('alert-success', 'Data imported successfully!!');
    				}
    				else{
    					session()->flash('alert-danger', 'An error occured. Please try again.');
    				}

    			}

    		}

    	}
        return back();

    }
}
