<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BrokerageCharges;

use Carbon\Carbon;

class BrokerageChargesController extends Controller
{

    public function __construct(){

        $this->middleware('auth');

    }

    public function index(Request $request){

    	$cash = BrokerageCharges::paginate(10);

    	return view('brokerages.view', compact('cash'))->with('i', ($request->input('page', 1)-1)*10);

    }


    public function update($id){

    	$this->validate(request(),[

    		'inputUpdateRate'=>'required'
    	]);

    	$update = BrokerageCharges::find($id);

    	$update->rate = request('inputUpdateRate');

    	$update->modified_ip = \Request::ip();

    	$update->updated_at = Carbon::now()->toDateTimeString();

    	$update->save();

    	session()->flash('alert-success', 'Data updated successfully!');

    	return back();

    }

}
