<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transactions;

class TransactionsController extends Controller
{
    public function index(Request $request){
    	$data = Transactions::paginate(10);

    	return view('/transaction.view', compact('data'))->with('i', ($request->input('page', 1)-1)*10);
    }
}
