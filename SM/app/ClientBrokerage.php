<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientBrokerage extends Model
{
    protected $fillable = ['client_id', 'account_id', 'cash_delivery_rate', 'cash_intraday_rate', 'future_rate', 'option_rate', 'currency_future_rate', 'currency_option_rate', 'commodities', 'wef', 'created_ip'];
}
