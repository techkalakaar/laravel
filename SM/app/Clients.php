<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    protected $fillable = [
    	'client_id', 'account_id', 'name', 'number', 'email', 'city', 'investment_amount', 'investment_date', 'created_ip', 'modified_ip'
    ];
}
