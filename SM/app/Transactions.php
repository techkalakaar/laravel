<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    protected $fillable = ['id', 'user_id', 'account_id', 'segment', 'buy_date', 'buy_or_sell', 'qty', 'script_name', 'purchase_rate', 'purchase_tax', 'sell_date', 'sell_rate', 'sell_tax', 'open_or_close', 'sl_or_trail', 'target', 'margin', 'profit_or_loss', 'timeframe', 'created_ip', 'created_at'];
}
