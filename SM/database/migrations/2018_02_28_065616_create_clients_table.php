<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('client_id');
            $table->string('account_id')->unique();
            $table->string('name');
            $table->string('number');
            $table->string('email');
            $table->string('city');
            $table->bigInteger('investment_amount');
            $table->date('investment_date');
            $table->string('created_ip');
            $table->string('modified_ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
