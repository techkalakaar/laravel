<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('exchange_segment');
            $table->string('account_id');
            $table->string('symbol');
            $table->string('expiry_date');
            $table->string('strike_price');
            $table->string('option_type');
            $table->string('buy_or_sell');
            $table->string('product_type');
            $table->string('trade_qty');
            $table->string('trade_price');
            $table->string('trade_status');
            $table->integer('status');
            $table->date('deal_date');
            $table->string('created_ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deals');
    }
}
