<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientBrokeragesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_brokerages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->string('account_id');
            $table->string('cash_delivery_rate');
            $table->string('cash_intraday_rate');
            $table->string('future_rate');
            $table->string('option_rate');
            $table->string('currency_future_rate');
            $table->string('currency_option_rate');
            $table->string('commodities');
            $table->date('wef');
            $table->string('created_ip');      
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_brokerages');
    }
}
