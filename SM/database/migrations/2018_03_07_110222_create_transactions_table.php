<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('account_id');
            $table->string('segment')->nullable();
            $table->string('buy_date')->nullable();
            $table->string('buy_or_sell')->nullable();
            $table->string('qty')->nullable();
            $table->string('script_name')->nullable();
            $table->string('purchase_rate')->nullable();
            $table->string('purchase_tax')->nullable();
            $table->string('sell_date')->nullable();
            $table->string('sell_rate')->nullable();
            $table->string('sell_tax')->nullable();
            $table->string('open_or_close')->nullable();
            $table->string('sl_or_trail')->nullable();
            $table->string('target')->nullable();
            $table->string('margin')->nullable();
            $table->string('profit_or_loss')->nullable();
            $table->string('timeframe')->nullable();
            $table->string('created_ip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
