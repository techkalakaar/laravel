-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 07, 2018 at 02:56 AM
-- Server version: 5.6.36-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `goldtree`
--

-- --------------------------------------------------------

--
-- Table structure for table `gt_brokerage_charges`
--

CREATE TABLE IF NOT EXISTS `brokerage_charges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) NOT NULL,
  `name` varchar(500) NOT NULL,
  `rate` varchar(200) NOT NULL,
  `levied_on` varchar(500) NOT NULL,
  `created_ip` varchar(200) NOT NULL,
  `modified_ip` varchar(200) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=75 ;

--
-- Dumping data for table `gt_brokerage_charges`
--

INSERT INTO `brokerage_charges` (`id`, `type`, `name`, `rate`, `levied_on`, `created_ip`, `modified_ip`, `created_at`, `updated_at`) VALUES
(1, 'Cash', 'GST(CGST,SGST,IGST &UTT)', '18', 'Brkg + Tran.Chgs+SEBI FEES', '127.0.0.1', '127.0.0.1', '2017-10-03 23:02:09', '2017-10-03 23:02:09'),
(2, 'Cash', 'Stamp Duty# (Square off)', '0.002', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(3, 'Cash', 'Stamp Duty# (Delivery)', '0.01', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(4, 'Cash', 'Transaction Charges (NSE)', '0.00325', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(5, 'Cash', 'Transaction Charges (BSE)', '0.00275', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(74, 'Cash', 'Exit Load', '18', '', '127.0.0.1', '127.0.0.1', '2017-10-10 06:17:16', '2017-10-10 06:17:16'),
(72, 'Cash', 'Delivery Brokerage', '0.30', 'Turnover', '127.0.0.1', '127.0.01', '2017-10-10 06:17:16', '2017-10-10 06:17:16'),
(73, 'Cash', 'Intraday Brokerage', '0.03', 'Turnover', '127.0.0.1', '127.0.0.1', '2017-10-10 06:17:16', '2017-10-10 06:17:16'),
(13, 'Cash', 'Transaction Charges (BSE)(On OFS and OTB)', '0.003', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(14, 'Cash', 'Transaction Charges (BSE)(On SS & ST)', '1', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(15, 'Cash', 'Securities Trans.Tax (Delivery)', '0.10', 'Buy / Sale', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(16, 'Cash', 'Securities Trans.Tax (Square Off)', '0.025', 'Sale only', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(17, 'Cash', 'Securities Trans.Tax (Equity Oriented Funds)', '0.001', 'Sale only', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(18, 'Cash', 'Sebi Turnover Fees', '0.00015', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(19, 'Derivatives', 'GST(CGST,SGST,IGST &UTT)*', '18%', 'Brkg + Tran.Chgs+SEBI FEES', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(20, 'Derivatives', 'Stamp Duty# (In case of option on premium turnover value)', '0.002', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(21, 'Derivatives', 'Transaction Charges (NSE Future)', '0.0019', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(22, 'Derivatives', 'Transaction Charges (NSE Option) on Premium Turnover', '0.05', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(23, 'Derivatives', 'Transaction Charges on Active Order (BSE Future) on Trade Value', '0.0005', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(24, 'Derivatives', 'Transaction Charges on Active Order (BSE Option) on Premium Value', '0.0005', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(25, 'Derivatives', 'STT on Sale of an option in securities on premium price', '0.05', 'Sale only', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(26, 'Derivatives', 'STT Sale of an option in securities, where option is exercised on setl price', '0.125', 'Purchase only', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(27, 'Derivatives', 'STT on Sale of a futures in securities', '0.01', 'Sale only', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(28, 'Derivatives', 'Sebi Turnover Fees', '0.00015', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(29, 'PCM', 'GST(CGST,SGST,IGST &UTT)*', '18%', 'Brkg + Tran.Chgs+SEBI FEES', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(30, 'PCM', 'Transaction Charges (NCD Future)', '0.0009', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(31, 'PCM', 'Transaction Charges (NCD Interest Rate Future)', '0.000155', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(32, 'PCM', 'Transaction Charges (NCD Option) on Premium Turnover', '0.04', 'Premium Turnover', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(33, 'PCM', 'Transaction Charges (MCD Future)', 'Refer Sheet MCX SX Trnc Chrg', '', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(34, 'PCM', 'Transaction Charges (MCD Option) on Premium Turnover', '0.03', 'Premium Turnover', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(35, 'PCM', 'Transaction Charges on (BSE CD Future) on Trade Value', '', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(36, 'PCM', 'From November 01,2016 to December 31, 2016', 'Rs.12/- Per crore', '', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(37, 'PCM', 'From? January 01,2017 to February 28, 2017', 'Rs.15/- Per crore', '', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(38, 'PCM', 'From March 01,2017 to April 30, 2017', 'Rs.18/- Per crore', '', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(39, 'PCM', 'From May 01,2017 to June 30,2017', 'Rs.22/- Per crore', '', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(40, 'PCM', 'From July 01, 2017onwards.', 'Rs.22/- Per crore', '', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(41, 'PCM', 'Transaction Charges on (BSE CD Option) on Premium Value', '', 'Premium Turnover', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(42, 'PCM', 'From November 01,2016 to December 31, 2016', 'Rs.5/- Per lac', '', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(43, 'PCM', 'From? January 01,2017 to February 28, 2017', 'Rs.10/- Per lac', '', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(44, 'PCM', 'From March 01,2017 to April 30, 2017', 'Rs.15/- Per lac', '', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(45, 'PCM', 'From May 01,2017 to June 30,2017', 'Rs.20/- Per lac', '', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(46, 'PCM', 'From July 01, 2017onwards.', 'Rs.20/- Per lac', '', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(47, 'PCM', 'Stamp Duty# (In case of option on premium turnover value)', '0.002', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(48, 'PCM', 'SEBI Turnover Fees in Futures', '0.00015', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(49, 'PCM', 'SEBI Turnover Fees on Interest Rate Futures (IRF)', '0.00005', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(50, 'PCM', 'SEBI Turnover Fees in Option on Premium Turnover', '0.00015', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(51, 'Commodities', 'GST(CGST,SGST,IGST &UTT)*', '18%', 'Brkg + Tran.Chgs+Risk Management Fee+SEBI FEES', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(52, 'Commodities', 'GST(CGST,SGST,IGST &UTT)*', '18%', 'Brkg + Tran.Chgs+SEBI FEES', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(53, 'Commodities', 'Stamp Duty#', '0.001', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(54, 'Commodities', 'Transaction Charges (ACE)', '0.001', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(55, 'Commodities', 'Transaction Charges (MCx)        List A Commodities    (as per Sheet4)', '0.0026', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(56, 'Commodities', 'Transaction Charges (MCx)        List B Commodities    (as per Sheet4)', '0.00175', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(57, 'Commodities', 'Transaction Charges (NCDEx)    List A Commodities    (as per Sheet3)', '0.004', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(58, 'Commodities', 'Transaction Charges (NCDEx)    List B Commodities    (as per Sheet3)', '0.002', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(59, 'Commodities', 'Transaction Charges (NCDEx)    List C Commodities    (as per Sheet3)', '0.0001', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(60, 'Commodities', 'Commodity Transaction Tax (NCDEx/MCx/ACE)', '0.01', 'Sale Only', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(61, 'Commodities', 'Transaction Charges (National Spot Ex.Future) ', 'As per spot commodity', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(62, 'Commodities', 'Risk Management Fee (NCDEX)', '0.004', 'Incremental Open Position', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00'),
(63, 'Commodities', 'Sebi Turnover Fees', '0.00015', 'Turn Over', '127.0.0.1', '127.0.0.1', '2003-10-17 23:02:00', '2003-10-17 23:02:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
