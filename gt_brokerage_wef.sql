-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Mar 07, 2018 at 02:24 AM
-- Server version: 5.6.36-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `goldtree`
--

-- --------------------------------------------------------

--
-- Table structure for table `gt_brokerage_wef`
--

CREATE TABLE IF NOT EXISTS `gt_brokerage_wef` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brokerage_id` int(11) NOT NULL,
  `rate` varchar(200) NOT NULL,
  `wef` date NOT NULL,
  `created_ip` varchar(200) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=68 ;

--
-- Dumping data for table `gt_brokerage_wef`
--

INSERT INTO `brokerage_wef` (`id`, `brokerage_id`, `rate`, `wef`, `created_ip`, `created_at`) VALUES
(1, 1, '18', '2017-10-10', '115.97.48.103', '2005-10-17 00:40:00'),
(2, 2, '0.002', '2017-10-10', '127.0.0.1', '2006-10-17 04:09:00'),
(3, 3, '0.01', '2017-10-10', '42.109.2.129', '2005-10-17 12:02:00'),
(4, 4, '0.00325', '2017-10-10', '42.109.2.130', '2006-10-17 12:02:00'),
(5, 5, '0.00275', '2017-10-10', '42.109.2.131', '2007-10-17 12:02:00'),
(67, 74, '18', '2017-10-10', '127.0.0.1', '2017-10-10 06:17:16'),
(66, 73, '0.03', '2017-10-10', '127.0.0.1', '2017-10-10 06:17:16'),
(65, 72, '0.30', '2017-10-10', '127.0.0.1', '2017-10-10 06:17:16'),
(13, 13, '0.003', '2017-10-10', '42.109.2.139', '2015-10-17 12:02:00'),
(14, 14, '1', '2017-10-10', '42.109.2.140', '2016-10-17 12:02:00'),
(15, 15, '0.10', '2017-10-10', '42.109.2.141', '2017-10-17 12:02:00'),
(16, 16, '0.025', '2017-10-10', '42.109.2.142', '2018-10-17 12:02:00'),
(17, 17, '0.001', '2017-10-10', '42.109.2.143', '2019-10-17 12:02:00'),
(18, 18, '0.00015', '2017-10-10', '42.109.2.144', '2020-10-17 12:02:00'),
(19, 19, '18%', '2017-10-10', '42.109.2.145', '2021-10-17 12:02:00'),
(20, 20, '0.002', '2017-10-10', '42.109.2.146', '2022-10-17 12:02:00'),
(21, 21, '0.0019', '2017-10-10', '42.109.2.147', '2023-10-17 12:02:00'),
(22, 22, '0.05', '2017-10-10', '42.109.2.148', '2024-10-17 12:02:00'),
(23, 23, '0.0005', '2017-10-10', '42.109.2.149', '2025-10-17 12:02:00'),
(24, 24, '0.0005', '2017-10-10', '42.109.2.150', '2026-10-17 12:02:00'),
(25, 25, '0.05', '2017-10-10', '42.109.2.151', '2027-10-17 12:02:00'),
(26, 26, '0.125', '2017-10-10', '42.109.2.152', '2028-10-17 12:02:00'),
(27, 27, '0.01', '2017-10-10', '42.109.2.153', '2029-10-17 12:02:00'),
(28, 28, '0.00015', '2017-10-10', '42.109.2.154', '2030-10-17 12:02:00'),
(29, 29, '18%', '2017-10-10', '42.109.2.155', '2031-10-17 12:02:00'),
(30, 30, '0.0009', '2017-10-10', '42.109.2.156', '2001-11-17 12:02:00'),
(31, 31, '0.000155', '2017-10-10', '42.109.2.157', '2002-11-17 12:02:00'),
(32, 32, '0.04', '2017-10-10', '42.109.2.158', '2003-11-17 12:02:00'),
(33, 33, 'Refer Sheet MCX SX Trnc Chrg', '2017-10-10', '42.109.2.159', '2004-11-17 12:02:00'),
(34, 34, '0.03', '2017-10-10', '42.109.2.160', '2005-11-17 12:02:00'),
(35, 35, '', '2017-10-10', '42.109.2.161', '2006-11-17 12:02:00'),
(36, 36, 'Rs.12/- Per crore', '2017-10-10', '42.109.2.162', '2007-11-17 12:02:00'),
(37, 37, 'Rs.15/- Per crore', '2017-10-10', '42.109.2.163', '2008-11-17 12:02:00'),
(38, 38, 'Rs.18/- Per crore', '2017-10-10', '42.109.2.164', '2009-11-17 12:02:00'),
(39, 39, 'Rs.22/- Per crore', '2017-10-10', '42.109.2.165', '2010-11-17 12:02:00'),
(40, 40, 'Rs.22/- Per crore', '2017-10-10', '42.109.2.166', '2011-11-17 12:02:00'),
(41, 41, '', '2017-10-10', '42.109.2.167', '2012-11-17 12:02:00'),
(42, 42, 'Rs.5/- Per lac', '2017-10-10', '42.109.2.168', '2013-11-17 12:02:00'),
(43, 43, 'Rs.10/- Per lac', '2017-10-10', '42.109.2.169', '2014-11-17 12:02:00'),
(44, 44, 'Rs.15/- Per lac', '2017-10-10', '42.109.2.170', '2015-11-17 12:02:00'),
(45, 45, 'Rs.20/- Per lac', '2017-10-10', '42.109.2.171', '2016-11-17 12:02:00'),
(46, 46, 'Rs.20/- Per lac', '2017-10-10', '42.109.2.172', '2017-11-17 12:02:00'),
(47, 47, '0.002', '2017-10-10', '42.109.2.173', '2018-11-17 12:02:00'),
(48, 48, '0.00015', '2017-10-10', '42.109.2.174', '2019-11-17 12:02:00'),
(49, 49, '0.00005', '2017-10-10', '42.109.2.175', '2020-11-17 12:02:00'),
(50, 50, '0.00015', '2017-10-10', '42.109.2.176', '2021-11-17 12:02:00'),
(51, 51, '18%', '2017-10-10', '42.109.2.177', '2022-11-17 12:02:00'),
(52, 52, '18%', '2017-10-10', '42.109.2.178', '2023-11-17 12:02:00'),
(53, 53, '0.001', '2017-10-10', '42.109.2.179', '2024-11-17 12:02:00'),
(54, 54, '0.001', '2017-10-10', '42.109.2.180', '2025-11-17 12:02:00'),
(55, 55, '0.0026', '2017-10-10', '42.109.2.181', '2026-11-17 12:02:00'),
(56, 56, '0.00175', '2017-10-10', '42.109.2.182', '2027-11-17 12:02:00'),
(57, 57, '0.004', '2017-10-10', '42.109.2.183', '2028-11-17 12:02:00'),
(58, 58, '0.002', '2017-10-10', '42.109.2.184', '2029-11-17 12:02:00'),
(59, 59, '0.0001', '2017-10-10', '42.109.2.185', '2030-11-17 12:02:00'),
(60, 60, '0.01', '2017-10-10', '42.109.2.186', '2001-12-17 12:02:00'),
(61, 61, 'As per spot commodity', '2017-10-10', '42.109.2.187', '2002-12-17 12:02:00'),
(62, 62, '0.004', '2017-10-10', '42.109.2.188', '2003-12-17 12:02:00'),
(63, 63, '0.00015', '2017-10-10', '42.109.2.189', '2004-12-17 12:02:00'),
(64, 1, '12.05', '2017-10-10', '115.97.48.103', '2017-10-06 20:19:45');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
